﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace nvidiascr
{
    public partial class ScreenSaverForm : Form
    {
        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);
        [DllImport("user32.dll")]
        static extern int SetWindowLong(IntPtr hWnd, int nIndex, IntPtr dwNewLong);
        [DllImport("user32.dll", SetLastError = true)]
        static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        static extern bool GetClientRect(IntPtr hWnd, out Rectangle lpRect);
        private bool previewMode = false;
        private Point mouseLocation;
        int xDirection = 1, yDirection = 1;

        public ScreenSaverForm()
        {
            InitializeComponent();
        }

        public ScreenSaverForm(Rectangle Bounds)
        {
            InitializeComponent();
            this.Bounds = Bounds;
        }

        public ScreenSaverForm(IntPtr PreviewWndHandle)
        {
            InitializeComponent();

            // Set preview window handle as parent of the window
            SetParent(this.Handle, PreviewWndHandle);

            // Close when parent closes
            SetWindowLong(this.Handle, -16, new IntPtr(GetWindowLong(this.Handle, 16) | 0x40000000));

            // Place this window inside parent
            Rectangle parentRect;
            GetClientRect(PreviewWndHandle, out parentRect);
            this.Size = parentRect.Size;
            this.Location = new Point(0, 0);

            // Set us to preview mode
            previewMode = true;
        }

        private void ScreenSaverForm_Load(object sender, EventArgs e)
        {
            Random rand = new Random((int) DateTime.Now.Ticks);

            // Hide cursor and set to top most
            Cursor.Hide();
            TopMost = true;

            // Set logo size according the resolution
            boxLogo.Size = new Size((Bounds.Width / 5), (int)(Bounds.Width / 5 * 0.738));

            // Check that we follow rules of minimum size of the logo
            if (boxLogo.Height < 38)
            {
                boxLogo.Size = new Size(38, 28);
            }

            // Move logo to random position
            boxLogo.Left = rand.Next(Math.Max(1, Bounds.Width - boxLogo.Width));
            boxLogo.Top = rand.Next(Math.Max(1, Bounds.Height - boxLogo.Height));

            // Random direction for movement
            xDirection = rand.Next(0, 2) * 2 - 1;
            yDirection = rand.Next(0, 2) * 2 - 1;

            // Set timer and start it..
            moveTimer.Interval = 55;
            moveTimer.Tick += new EventHandler(moveTimer_Tick);
            moveTimer.Start();

            // Unhide logo
            boxLogo.Visible = true;
        }
        
        private void moveTimer_Tick(object sender, EventArgs e)
        {
            if (boxLogo.Bottom >= Bounds.Height)
            {
                yDirection = -1;
            }
            else if (boxLogo.Top <= 0)
            {
                yDirection = 1;
            }

            if (boxLogo.Right >= Bounds.Width)
            {
                xDirection = -1;
            }
            else if (boxLogo.Left <= 0)
            {
                xDirection = 1;
            }

            boxLogo.Left = boxLogo.Left + xDirection;
            boxLogo.Top = boxLogo.Top + yDirection;

            this.Invalidate();
        }

        private void ScreenSaverForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (!previewMode)
            {
                if (!mouseLocation.IsEmpty)
                {
                    if (Math.Abs(mouseLocation.X - e.X) > 5 ||
                        Math.Abs(mouseLocation.Y - e.Y) > 5)
                    {
                        Application.Exit();
                    }
                }
                mouseLocation = e.Location;
            }
        }

        private void ScreenSaverForm_MouseClick(object sender, MouseEventArgs e)
        {
            if (!previewMode)
            {
                Application.Exit();
            }
        }

        private void ScreenSaverForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!previewMode)
            {
                Application.Exit();
            }
        }

        private void ScreenSaverForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!previewMode)
            {
                Application.Exit();
            }
        }
    }
}
