﻿using System;
using System.Windows.Forms;

namespace nvidiascr
{
    class Program
    {
        static string programName = "NVIDIA Screen Saver";

        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (args.Length > 0)
            {
                if (args[0].ToLower().Substring(0, 2) == "/s")
                {
                    // Show screensaver
                    ShowScreenSaver();
                    Application.Run();
                }
                else if (args[0].ToLower().Substring(0, 2) == "/p")
                {
                    // Show preview
                    if (args[1] != null)
                    {
                        IntPtr previewWndHandle = new IntPtr(long.Parse(args[1]));
                        Application.Run(new ScreenSaverForm(previewWndHandle));
                    }
                    else
                    {
                        Application.Exit();
                    }
                }
                else if (args[0].ToLower().Substring(0, 2) == "/c")
                {
                    MessageBox.Show("NVIDIA and the NVIDIA logo are trademarks and/or registered trademarks of NVIDIA Corporation in the U.S. and other countries. Other company and product names may be trademarks of the respective companies with which they are associated.\n\n"
                                    + "Screensaver written in C#, sources at \"http://git.lihis.net/Lihis/nvidiascr\".\n"
                                    + "This screensaver is intended for the all NVIDIA fanboys out there.\n\n"
                                    + "Version: " + nvidiascr.Properties.Resources.version + "\n\n"
                                    + "Written by Tomi Lähteenmäki, 2016", programName + " - Options");
                }
                else
                {
                    // Unknow parameter
                    MessageBox.Show("Unknow parameter \"" + args[0] + "\"", programName);
                }
            }
            else
            {
                // No arguments, show screen saver
                ShowScreenSaver();
                Application.Run();
            }
        }

        static void ShowScreenSaver()
        {
            foreach (Screen screen in Screen.AllScreens)
            {
                ScreenSaverForm screensaver = new ScreenSaverForm(screen.Bounds);
                screensaver.Show();
            }
        }
    }
}
