# nvidiascr

Screensaver where NVIDIA logo moves around the screen. 

## Installation

Compile or download binary

#### Compile

Clone sources, compile, rename compiled .exe to .scr, right-click, choose Install.

#### Binary

Download binary [nvidiascr.scr](http://git.lihis.net/Lihis/nvidiascr/raw/master/nvidiascr.scr), right-click it and choose Install.

## Notice

NVIDIA and the NVIDIA logo are trademarks and/or registered trademarks of NVIDIA Corporation in the U.S. and other countries. Other company and product names may be trademarks of the respective companies with which they are associated.
